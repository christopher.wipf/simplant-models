# Simulated plant models
## arm_cavity
This model includes two quadruple pendulum suspensions, global cavity DOFs, and radiation pressure.

## mode_cleaner
This model includes three single pendulum suspensions, and global cavity DOFs.
