void damped_quad_rtss_modal_itmx(double *in, int ins, double *y, int outs)
{
  /* state space vars */
#include "damped_quad_rtss_modal_itmx.h"

  static int error = 0;
  double *u;

  /* argument check */
  if ((ins != len_u + 1) || (outs != len_y)) {
    if (!(error & 0x1)) {
      printf("damped_quad_rtss_modal_itmx: wrong number of inputs and/or outputs\n");
      error |= 0x1;
    }
    return;
  }

  /* in[0] is used to request to clear state */
  u = in;
  u++;
  if (in[0] == 1.0) {
    for (int i = 0; i < len_x; i++) {
      x[i] = 0;
    }
  }

  /* compute outputs */
  /* y(k) += C*x(k) */
  for (int i = 0; i < len_y; i++) {
    y[i] = 0;
    for (int j = 0; j < len_x; j++) {
      y[i] += C[i][j]*x[j];
    }
  }
  /* y(k) += D*u(k) */
  for (int i = 0; i < len_y; i++) {
    for (int j = 0; j < len_u; j++) {
      y[i] += D[i][j]*u[j];
    }
  }

  /* update state */
  /* x(k+1) += A*x(k) */
  for (int i = 0; i < len_x; i++) {
    xnew[i] = 0;
    for (int j = 0; j < len_x; j++) {
      xnew[i] += A[i][j]*x[j];
    }
  }
  /* x(k+1) += B*u(k) */
  for (int i = 0; i < len_x; i++) {
    for (int j = 0; j < len_u; j++) {
      xnew[i] += B[i][j]*u[j];
    }
  }
  for (int i = 0; i < len_x; i++) {
    x[i] = xnew[i];
  }
}

